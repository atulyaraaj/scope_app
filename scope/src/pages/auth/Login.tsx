import {
  Box,
  Button,
  Card,
  Flex,
  Heading,
  Text,
  TextField,
} from "@radix-ui/themes";
import { useFrappeAuth } from "frappe-react-sdk";
import { useState } from "react";

const Login = () => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const { login, isLoading, currentUser} = useFrappeAuth();

  const onSubmit = () => {
    login({
      username: username,
      password: password,
    });
  };

  return (
    <div>
      <Flex justify="center" align="center" className="w-screen h-screen">
        <Card className="w-[40vw]">
          <Heading>Login</Heading>
          <p>{currentUser}</p>
          <Flex direction="column" gap="2">
            <Box>
              <Text as="label">Username</Text>
              <TextField.Root
                placeholder="username"
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
                value={username}
              />
            </Box>
            <Box>
              <Text as="label">Pzssword</Text>
              <TextField.Root
                placeholder="********************"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                value={password}
              />
            </Box>
            <Button onClick={onSubmit} disabled={isLoading}>
              Login
            </Button>
          </Flex>
        </Card>
      </Flex>
    </div>
  );
};

export default Login;
