import { FrappeProvider } from "frappe-react-sdk";
import "@radix-ui/themes/styles.css";
import { Theme } from "@radix-ui/themes";
import Login from "./pages/auth/Login";

function App() {
  return (
    <div className="App">
      <Theme accentColor="crimson">
        <FrappeProvider
          socketPort={import.meta.env.VITE_SOCKET_PORT}
          siteName={import.meta.env.VITE_SITE_NAME}
        >
          <Login />
          

          
        </FrappeProvider>
      </Theme>
    </div>
  );
}

export default App;
